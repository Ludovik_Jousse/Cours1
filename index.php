<?php

// Déclaration
// Class qui va représenter un stylo sous forme de code
class pen
{
    public string $color;
    private string $brand;

    // création du constructeur
    public function __construct( string $c_brand, $c_color = 'black')
    {
        //this représente l'objet sur lequel on est en train d'agir
        $this->brand = $c_brand;
        $this->color = $c_brand;
    }

}

//utilisation
$bic_noir = new pen('Bic');

var_dump($bic_noir);